import requests
from bs4 import BeautifulSoup
from random import choice
from random import uniform
from time import sleep

def get_html(url, user_agent, proxy):
    print('get_html')
    response = requests.get(url, headers=user_agent, proxies=proxy)
    return response.text


def get_ip(html):
    
    if not html:
        print('html is None')
        return

    print('New proxy & user-agent:')

    soup = BeautifulSoup(html, 'lxml')

    ip = soup.find('span', class_='ip')
    print ('Test message')

    if ip:
        text_ip = ip.text.strip()
        user_agent = soup.find('span', class_='ip').find_next_sibling('span').text.strip()
        
        print('ip = ' + text_ip)
        print('user_agent = ' + user_agent)
    else:
        print('ip = ' + str(ip))

    print('-' * 100)

def main():
    url = 'http://www.sitespy.ru/my-ip'

    user_agents = open('user_agents.txt').read().split('\n')
    proxies = open('proxies.txt').read().split('\n')

    for i in range(10):
        sleep(uniform(0, i))
        proxy = {'http': 'http://' + choice(proxies)}
        user_agent = {'user-agent': choice(user_agents)}

        print('proxy = ' + str(proxy))
        print('user_agent =' + str(user_agent))

        try:
            html = get_html(url, user_agent, proxy)
        except:
            continue
            
        get_ip(html)


if __name__ == '__main__':
    main()